import React, { Component } from 'react';
import { Container } from 'native-base';
import { connect } from 'react-redux';

import CustomHeader from '../../components/CustomHeader';
import ProductsList from '../../components/ProductsList';
import ShopHeader from '../../components/ShopHeader';
import Product from '../../interfaces/Product';
import { setProducts } from '../../actions/products';
import { IProductsState } from '../../reducers/products';

const data = require('../../../assets/mock/shoes-mock.json');

class HomeScreen extends Component<Props> {
	componentDidMount() {
		const { setProducts } = this.props;
	
		setProducts(data);
	}

	_handlePressProductItem = (data: Product) => {
		this.props.navigation.navigate('ProductDetailsScreen', { data });
	}

	render() {
		const { products } = this.props;

		return (
			<Container>
				<CustomHeader
					title="Sneakers!!!"
				/>

				<ShopHeader />
				
				<ProductsList
					products={products}
					handlePressItem={this._handlePressProductItem}
				/>

			</Container>
		)
	}
}

interface Props {
	navigation: any
	products: Product[],
	setProducts: (data: Product[]) => void,
};

const mapStateToProps = ({ products }: { products: IProductsState }) => {
	return {
		products: products.data,
	};
};

const mapDispatchToProps = (dispatch: (action: any) => any) => {
	return {
		setProducts: (data: Product[]) => { dispatch(setProducts(data)) }
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
