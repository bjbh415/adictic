import React, { Component } from 'react';
import {
	Dimensions,
	StyleSheet,
	View,
} from 'react-native';
import {
	Container,
} from 'native-base';
import { SwipeListView } from 'react-native-swipe-list-view';
import Product from '../../interfaces/Product';
import CartItem from '../../components/CartItem';
import CustomHeader from '../../components/CustomHeader';
import { Row, Col } from 'react-native-easy-grid';

import CustomIcon from '../../components/CustomIcon';
import { TouchableOpacity } from 'react-native-gesture-handler';

const data = require('../../../assets/mock/shoes-mock.json');

class CartScreen extends Component<Props, State> {
	constructor(props: Props) {
    super(props);
		
		this.state = {
			// listViewData: props.products,
			listViewData: data,
		};
  }

	render() {
		const { navigation } = this.props;

		return (
			<Container>
				<CustomHeader
					title="Carrito"
					goBack={() => { navigation.goBack() }}
				/>

        <SwipeListView
					data={this.state.listViewData}
					renderItem={(data, rowMap) => (
						<CartItem
							product={data.item}
						/>
					)}
					renderHiddenItem={ (data, rowMap) => (
						<Row style={{ justifyContent: 'space-between', paddingVertical: 8, paddingHorizontal: 2 }}>
							<Col style={{ backgroundColor: '#ff94c2', justifyContent: 'center', alignItems: 'flex-start' }}>
								<TouchableOpacity style={{ padding: 20 }}>
									<CustomIcon
										name="close"
										style={{ color: "#fff" }}
									/>
								</TouchableOpacity>
							</Col>
							<Col style={{ backgroundColor: '#ff94c2', justifyContent: 'center', alignItems: 'flex-end' }}>
								<TouchableOpacity style={{ padding: 20 }}>
									<CustomIcon
										name="close"
										style={{ color: "#fff" }}
									/>
								</TouchableOpacity>
							</Col>
						</Row>
					)}
					leftOpenValue={75}
					rightOpenValue={-75}
					keyExtractor={(item) => item._id}
        />
      </Container>

		);
	}
}


const { width } = Dimensions.get('screen');
const styles = StyleSheet.create({});

interface Props {
	navigation: any,
	products: Product[],
};
interface State {
	listViewData: Product[],
};


export default CartScreen;
