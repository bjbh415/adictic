import React, { PureComponent } from 'react';
import {
	StyleSheet,
	Dimensions,
} from 'react-native';
import {
	Container,
	H1,
	H2,
	View,
	Button,
	Text,
} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

import CustomIcon from '../../components/CustomIcon';

class WelcomeScreen extends PureComponent<Props> {
	_handleStart = () => {
			const { navigation } = this.props;
			navigation.navigate('HomeScreen');
	}

	render() {
		return (
			<Container>
				<LinearGradient
					colors={['#ff94c2', '#f06292']}
					style={[
						styles.center,
						styles.fullscreen,
					]}
				>
					<CustomIcon
						name="adictic-logo"
						size={120}
						style={[
							styles.fontColor,
						]}
					/>
					
					<View style={{ height: height * 0.02 }} />
					
					<H2
						style={[
							styles.fontColor,
							styles.fontBold,
						]}
					>
						adictic
					</H2>
					
					<View style={{ height: height * 0.3 }} />
					
					<H1
						style={[
							styles.fontColor,
							styles.fontBold,
						]}
					>
						TIENDA
					</H1>
					
					<View style={{ height: height * 0.02 }} />

					<Button
						transparent
						onPress={this._handleStart}
					>
						<Text
							style={[
								styles.fontColor,
							]}
						>
							¡Toca aquí para Empezar!
						</Text>
					</Button>

				</LinearGradient>
			</Container>		
		)
	}
}

const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
	center: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	fullscreen: {
		width,
		height,
	},
	fontColor: {
		color: '#fff',
		opacity: .5,
	},
	fontBold: {
		fontWeight: 'bold'
	}
});

interface Props {
  navigation: any
};

export default WelcomeScreen;
