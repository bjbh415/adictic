import React, { Component } from 'react';
import {
	StyleSheet,
	Dimensions,
	Image,
} from 'react-native';
import {
	Container, View, H3, Content, Text, Grid, Button, Form, Item, Input,
} from 'native-base';
import Carousel from 'react-native-snap-carousel';
import NumericInput from 'react-native-numeric-input'
import StarRating from 'react-native-star-rating';
import { connect } from 'react-redux';

import CustomHeader from '../../components/CustomHeader';
import Product from '../../interfaces/Product';
import { Row, Col } from 'react-native-easy-grid';
import Price from '../../components/Price';
import GeneralRating from '../../components/GeneralRating';
import CustomColorPicker from '../../components/CustomColorPicker';
import CustomSizePicker from '../../components/CustomSizePicker';
import CustomButton from '../../components/CustomButton';
import ProductCard from '../../components/ProductCard';
import RatingTable from '../../components/RatingTable';
import ProductCommentsList from '../../components/ProductCommentsList';
import {
	setSelectedProduct,
	setSelectedProductReview,
	setSelectedProductColor,
	setSelectedProductSize,
	setSelectedProductNumber,
} from '../../actions/products';
import {
	IProductReview,
	IProductsState,
	ISelectedProduct,
} from '../../reducers/products';

const data = require('../../../assets/mock/shoes-mock.json');

class ProductDetailsScreen extends Component<Props> {
	constructor(props: Props) {
		super(props);

		this.state = {
			rating: 0,
			title: '',
			comment: '',
		}
	}


	_carousel: any;

	componentDidMount() {
		const { setSelectedProduct, navigation } = this.props;
		const product: Product = navigation.getParam('data');
		
		setSelectedProduct(product._id);
	}

	_renderSlide = ({ item }: { item:string; index: number; }) => (
		<Image
			source={{ uri: item }}
			style={{
				backgroundColor: 'red',
				height: height * 0.4,
				width: width * 0.85,
				borderRadius: 12,
			}}
			resizeMode="cover"
		/>
	);

	_renderCard = ({ item }: { item:Product; index: number; }) => (
		<ProductCard
			imgUri={item.imgUri}
			title={item.title}
			onPress={() => {}}
			price={item.price}
			discountPrice={item.discountPrice}
		/>
	);
	
	render() {
		const { navigation } = this.props;
		const product: Product = navigation.getParam('data');

		return (
			<Container>
				<CustomHeader
					title="Sneakers!!!"
					goBack={() => { navigation.goBack() }}
					subtitle={product.category}
				/>

				<Content>
					<Carousel
						ref={(c: any) => { this._carousel = c; }}
						data={product.photos}
						renderItem={this._renderSlide}
						sliderWidth={width}
						sliderHeight={height * 0.4}
						itemWidth={width * 0.85}
						itemHeight={height * 0.4}
						style={{ backgroundColor: 'red' }}
						containerCustomStyle={{ marginVertical: 15 }}
					/>

					{ this.renderDescriptionSection(product) }
					{ this.renderColorPickerSection(product) }
					{ this.renderSizePickerSection(product) }
					{ this.renderAddToCartSection(product) }
					{ this.renderCommissionSection(product) }
					{ this.renderOtherProdsSection() }
					{ this.renderRatingSection(product) }
					{ this.renderReviewSection(product) }
					{ this.renderCommentsSection(product) }
				</Content>
			</Container>
		)
	}

	// SECTIONS
	// ================================================

	// Description
	private renderDescriptionSection(product: Product) {
		return <View style={styles.innerContainer}>
			<H3>{product.title}</H3>

			<Row style={{ paddingVertical: 12 }}>
				<Col>
					<GeneralRating rating={4} valorationsNumber={1000} />
				</Col>
				<Col>
					<Price price={product.price} discountPrice={product.discountPrice} />
				</Col>
			</Row>

			<Text note>
				{product.description}
			</Text>
		</View>;
	}
	// Colors
	private renderColorPickerSection(product: Product) {
		const {
			selectedProduct: { selectedColor },
			setSelectedProductColor,
		} = this.props;

		return <View style={styles.innerContainer}>
			<H3>Elige un color</H3>
			<View style={{ height: 15 }} />
			<CustomColorPicker
				colors={product.colors}
				selectedColor={selectedColor}
				onPress={setSelectedProductColor}
			/>
		</View>;
	}
	// Sizes
	private renderSizePickerSection(product: Product) {
		const {
			selectedProduct: { selectedSize },
			setSelectedProductSize,
		} = this.props;

		return <View style={styles.innerContainer}>
			<H3>Elige una talla</H3>
			<View style={{ height: 15 }} />
			<CustomSizePicker
				sizes={product.sizes}
				selectedSize={selectedSize}
				onPress={setSelectedProductSize}
			/>
		</View>;
	}
	
	// Add To Cart
	private renderAddToCartSection(product: Product) {
		const {
			selectedProduct: { selectedNumber },
			setSelectedProductNumber,
			navigation,
		} = this.props;

		return <View style={styles.innerContainer}>
			<Grid>
				<Col size={.4}>
					<NumericInput
						rounded
						minValue={0}
						maxValue={product.availableNumber}
						onChange={setSelectedProductNumber}
						// value={selectedNumber}
					/>
				</Col>
				<Col size={.6}>
					<CustomButton
						label="agragar"
						onPress={() => { navigation.navigate('CartScreen') }}
						/>
				</Col>
			</Grid>
		</View>;
	}

	// Sizes
	private renderCommissionSection(product: Product) {
		const { price, discountPrice } = product;
		const percentage: number = 6;
		const commission: number = discountPrice
			?	discountPrice * (percentage / 100) 
			: price * (percentage / 100);

		return <View style={styles.innerContainer}>
			<H3>Gane un porcentaje por colaboración</H3>
			<View style={{ height: 15 }} />
			<Button bordered light rounded style={{ width: 200 }}>
				<Text note>
					<Text style={[ styles.bold ]}>{percentage}%</Text>
					<Text> equivalente a </Text>
					<Text style={[ styles.bold ]}>{commission.toFixed(2)}$</Text>
				</Text>
			</Button>
			<View style={{ height: 15 }} />
			<Text note>
				cada vez que alguien compra este producto desde tus
				publicaciones guardadas.
			</Text>
		</View>;
	}

	// Other Products
	private renderOtherProdsSection() {
		return <View>
			<View style={styles.innerContainer}>
				<H3>Más productos de Massimo Dutti</H3>
			</View>
			<Carousel ref={(c: any) => { this._carousel = c; } } data={data} renderItem={this._renderCard} sliderWidth={width} sliderHeight={height * 0.4} itemWidth={width * 0.5} itemHeight={height * 0.4} style={{ backgroundColor: 'red' }} containerCustomStyle={{ marginVertical: 15 }} />
		</View>;
	}

	// Rating
	private renderRatingSection(product: Product) {
		return <View style={styles.innerContainer}>
			<H3>Valoraciones</H3>
			<View style={{ height: 15 }} />

			<RatingTable
				rating={product.rating}
			/>
		</View>
	}

	// Review
	private renderReviewSection(product: Product) {
		const { navigation } = this.props;

		return <View style={styles.innerContainer}>
			<H3>Reseña</H3>
			<Text>Toca para calificar</Text>
			<View style={{ height: 15 }} />

			<View style={styles.innerContainer}>
				<StarRating
					maxStars={5}
					rating={this.state.rating}
					starSize={48}
					fullStarColor="#FFE082"
					emptyStarColor="#ccc"
					selectedStar={(rating) => this.setState({ rating })}
				/>
			</View>

			<Form>
				<Item>
					<Input placeholder="Titulo (opcional)" />
				</Item>
				<Item last>
					<Input placeholder="Reseña (opcional)" />
				</Item>
			</Form>

			<View style={{ height: 15 }} />
			
			<CustomButton
				label="enviar"
				onPress={() => { }}
			/>

		</View>
	}

	// Comments
	private renderCommentsSection(product: Product) {
		return <View style={[
			styles.innerContainer,
			{ maxHeight: 200 }
		]}>
			<ProductCommentsList
				comments={product.comments}
			/>
		</View>
	}

}

const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
	innerContainer: {
		padding: 15,
	},
	bold: {
		fontWeight: 'bold',
	}
});

interface Props {
	navigation: any,
	products: Product[],
	selectedProduct: ISelectedProduct,
	setSelectedProduct: (_id: string) => void,
	setSelectedProductReview: (review: IProductReview) => void,
	setSelectedProductColor: (color: string) => void,
	setSelectedProductSize: (size: string) => void,
	setSelectedProductNumber: (number: number) => void,
};

const mapStateToProps = ({ products }: { products: IProductsState }) => {
	return {
		products: products.data,
		selectedProduct: products.selectedProduct,
	};
};

const mapDispatchToProps = (dispatch: (action: any) => any) => {
	return {
		setSelectedProduct: (_id: string) => { dispatch(setSelectedProduct(_id)) },
		setSelectedProductReview: (review: IProductReview) => { dispatch(setSelectedProductReview(review)) },
		setSelectedProductColor: (color: string) => { dispatch(setSelectedProductColor(color)) },
		setSelectedProductSize: (size: string) => { dispatch(setSelectedProductSize(size)) },
		setSelectedProductNumber: (number: number) => { dispatch(setSelectedProductNumber(number)) },
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailsScreen);
