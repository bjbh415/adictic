import React, { PureComponent } from 'react';
import { FlatList } from 'react-native-gesture-handler';

import ProductCommentItem from './ProductCommentItem';
import { ProductComment } from '../interfaces/Product';
	
export default class ProductCommentsList extends PureComponent<Props> {
	_keyExtractor = (item: ProductComment) => (item._id)

	_renderItem = ({ item }: ItemToRender) => (
		<ProductCommentItem
			_id={item._id}
			title={item.title}
			rating={item.rating}
			comment={item.comment}
		/>
	)
	
	render() {
		const { comments } = this.props;

		return (
			<FlatList<ProductComment>
				data={comments}
				renderItem={this._renderItem}
				keyExtractor={this._keyExtractor}
			/>
		);
	}
}

interface Props {
	comments: ProductComment[];
}

interface ItemToRender {
	item: ProductComment,
	index?: number,
}
