import React from 'react';
import {
	StyleSheet,
} from 'react-native';
import {
	Text,
	Button,
} from 'native-base';
export default ({
    label,
    onPress
}:CustomButtonProps) => (
	<Button
		block
		style={styles.button}
  	onPress={onPress}
	>
		<Text>{label}</Text>
	</Button>
);

const styles = StyleSheet.create({
	button: {
		backgroundColor: '#f06292',
		borderRadius: 12,
	}
});

interface CustomButtonProps {
    label: string,
    onPress: () => void
};
