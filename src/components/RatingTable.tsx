import React, { PureComponent } from 'react';
import {
  // Dimensions,
  StyleSheet,
} from 'react-native';
import {
  Text,
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import AnimatedBar from 'react-native-animated-bar';
import StarRating from 'react-native-star-rating';

export default class RatingTable extends PureComponent<RatingTableProps> {
  render() {
    const { rating } = this.props;
    const totalOfRating: number = rating.length;
    const totalAvg: number = (rating.reduce((a, b) => a + b, 0)) / totalOfRating;
    const getPercentage: (startsNum: number) => number = (startsNum) => 
      rating.filter((item: number) => item === startsNum).length / totalOfRating;

    return (
      <Grid>
        <Col
          size={0.3}
          style={styles.center}
        >
          <Text style={styles.ratingNumber}>{totalAvg.toFixed(1)}</Text>
          <Text style={styles.ratingCount}>de 5</Text>
        </Col>
        <Col
          size={0.7}
        >
          <Grid>
            {[5,4,3,2,1].map((item) => (
              this.RatingRow(item, getPercentage(item))
            ))}
            <Row>
              <Col size={0.4} />
              <Col size={0.6}>
                <Text
                  note
                  style={{ textAlign: 'right' }}
                >
                  {totalOfRating} calificaciones
                </Text>
              </Col>
            </Row>
          </Grid>
        </Col>
      </Grid>
    );
  }

  private RatingRow(stars: number, percentage: number) {
    return <Row size={.01} key={`${Math.random()}`}>
      <Col size={.4}>
        <StarRating containerStyle={styles.flexEnd} disabled maxStars={stars} rating={stars} starSize={12} fullStarColor="#ccc" />
      </Col>
      <Col size={0.05} />
      <Col size={.6} style={styles.center}>
        <AnimatedBar progress={percentage} height={5} borderColor="#fff" barColor="#ff94c2" fillColor="#ccc" borderRadius={5} borderWidth={0} animate={false} />
      </Col>
    </Row>;
  }
}

// const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
  center: {
		justifyContent: 'center',
		alignItems: 'center',
  },
  flexEnd: {
    justifyContent: 'flex-end',
		alignItems: 'center',
  },
  ratingNumber: {
    fontWeight: 'bold',
    fontSize: 40,
    color: '#383838'
  },
  ratingCount: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#484848'
  },
});

interface RatingTableProps {
	rating: number[],
};
