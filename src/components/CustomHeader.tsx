import React from 'react';
import {
	StyleSheet,
	Dimensions,
} from 'react-native';
import {
	Header,
	Button,
	Title,
	Subtitle,
} from 'native-base';
import { Col } from 'react-native-easy-grid';
import CustomIcon from './CustomIcon';

export default ({
	title,
	subtitle,
	goBack,	
}: CustomHeaderProps) => (
	<Header
		style={[
			styles.header,
		]}
	>
		<Col
			size={0.1}
			style={[
				styles.center,
			]}
		>
			{!!goBack && (
				<Button
					transparent
					onPress={goBack}
				>
					<CustomIcon
						name="arrow-left"
					/>
				</Button>
			)}
		</Col>

		<Col
			size={0.8}
			style={[
				styles.center,
			]}
		>
			<Title
				style={[
					styles.textColor,
					styles.fontBold,
				]}
			>
				{title}
			</Title>

			{!!subtitle && (
				<Subtitle
					style={[
						styles.textColor,
						styles.fontBold,
					]}
				>
					{subtitle}
				</Subtitle>
			)}
		</Col>
		
		<Col
			size={0.1}
			style={[
				styles.center,
			]}
		>
			{!true && (
				<Button
					transparent
					onPress={() => {}}
				>
					<CustomIcon
						name="arrow-left"
					/>
				</Button>
			)}
		</Col>
	</Header>
);

const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
	header: {
		backgroundColor: 'transparent',
	},
	center: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	fullscreen: {
		width,
		height,
	},
	textColor: {
		color: '#000',
		opacity: .8,
	},
	fontBold: {
		fontWeight: 'bold',
	}
});

interface CustomHeaderProps {
	title?: string,
	subtitle?: string,
	goBack?: () => void,
};
