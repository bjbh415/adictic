import React, { PureComponent } from 'react';
import {
  // Dimensions,
  StyleSheet,
  View,
  FlatList,
} from 'react-native';
import { Text } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class CustomSizePicker extends PureComponent<CustomSizeProps> {
  _renderSize = ({ item }: { item: string }) => (
    <TouchableOpacity
      onPress={() => this.props.onPress(item)}
    >
      <View
        style={[
          styles.sizeItemContainer,
          (this.props.selectedSize === item) ? styles.sizeActive : {},
        ]}
      >
        <Text>{item}</Text>
      </View>
    </TouchableOpacity>
  )

  render() {
    const { sizes } = this.props;
    
    return (
      <View>
        <FlatList<string>
          horizontal
          data={sizes}
          renderItem={this._renderSize}
          keyExtractor={(item) => item}
        />
      </View>
    );
  }
}

// const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
  sizeItemContainer: {
    margin: 2,
    width: 48,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    borderColor: '#ccc',
    borderStyle: "solid",
    borderWidth: 1,
  },
  sizeActive: {
    borderColor: '#f06292',
    borderWidth: 2,
  }
});

interface CustomSizeProps {
  sizes: string[],
  selectedSize?: string,
  onPress: (size: string) => void 
};
