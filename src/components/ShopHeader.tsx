import React from 'react';
import {
	StyleSheet,
	// Dimensions,
	Image,
} from 'react-native';
import {
	Card,
	CardItem,
	Text,
} from 'native-base';
	
export default () => (
	<Card
        transparent
	>
		<CardItem>
			<Image
				source={{uri: 'https://static1.abc.es/media/summum/2019/01/14/nikee-kK3B--620x349@abc.jpg'}}
				style={[ styles.image ]}
				resizeMode="cover"
			/>
		</CardItem>
        <CardItem
            footer
            style={styles.center}
        >
            <Text note>Las mejores zapatillas de Madrid</Text>
        </CardItem>
	</Card>
);

// const { width } = Dimensions.get('screen');
const styles = StyleSheet.create({
	center: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	image: {
		borderRadius: 16,
		width: '100%',
		height: 163,
	},
});
