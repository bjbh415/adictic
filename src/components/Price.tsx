import React from 'react';
import {
  // Dimensions,
  StyleSheet,
  View,
} from 'react-native';
import {
  Text
} from 'native-base';

export default ({
	price,
	discountPrice,
}: PriceProps) => (
  <View style={styles.container}>
    {discountPrice && (
      <>
        <Text
          style={[
            styles.font,
            styles.deprecated
          ]}
        >
          {`${price}$`}
        </Text>
        <View style={{ width: 16 }} />
        <Text
          style={[
            styles.font,
          ]}
        >
          {`${discountPrice}$`}
        </Text>
      </>
    )}
    {!discountPrice && (
      <Text
          style={[
            styles.font,
          ]}
        >
          {`${price}$`}
        </Text>
      )}
  </View>
);

// const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  font: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#f06292',
  },
  deprecated: {
		textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    color: '#bbb',
	}
});

interface PriceProps {
	price: number,
	discountPrice?: number,
};
