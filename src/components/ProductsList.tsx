import React, { PureComponent } from 'react';
import { FlatList } from 'react-native-gesture-handler';

import ProductCard from './ProductCard';
import Product from '../interfaces/Product';
	
export default class ProductsList extends PureComponent<Props> {
	_keyExtractor = (item: Product) => (item._id)

	_renderItem = ({ item }: ItemToRender) => (
		<ProductCard
			imgUri={item.imgUri}
			title={item.title}
			price={item.price}
			discountPrice={item.discountPrice}
			onPress={() => this.props.handlePressItem(item)}
		/>
	)
	
	render() {
		const { products } = this.props;

		return (
			<FlatList<Product>
				contentContainerStyle={{ alignItems: 'center' }}
				numColumns={2}
				data={products}
				renderItem={this._renderItem}
				keyExtractor={this._keyExtractor}
			/>
		);
	}
}

interface Props {
	products: Product[];
	handlePressItem: (data: Product) => void, 
}

interface ItemToRender {
	item: Product,
	index?: number,
}
