import React from 'react';
import {
  // Dimensions,
  StyleSheet,
  View,
} from 'react-native';
import {
	Text,
	ListItem,
	Left,
	Right,
	Body,
	Thumbnail,
	Card,
	CardItem,
	H2,
	H3,
} from 'native-base';
import Product from 'src/interfaces/Product';
import NumericInput from 'react-native-numeric-input';
import { Col, Row } from 'react-native-easy-grid';

export default ({
	product,
}: { product: Product }) => (
  <Card>
		<CardItem>
			<Col size={0.2}>
				<Thumbnail
					source={{uri: product.imgUri}}
					style={styles.thumb}
				/>
			</Col>
			<Col size={0.5}>
				<Text>{product.title}</Text>
				{product.discountPrice && (
					<Row>
						<H3
							style={styles.deprecated}
						>
							${product.price}
						</H3>

						<View style={{ width: 12 }} />
										
						<H3>${product.discountPrice}</H3>
					</Row>
				)}

				{!product.discountPrice && (
					<H3 note>{product.price}</H3>
				)}
			</Col>
			<Col size={0.32}>
				<NumericInput
					rounded
					minValue={0}
					maxValue={product.availableNumber}
					onChange={value => console.log(value)}
				/>
			</Col>
		</CardItem>
	</Card>
);

// const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
	deprecated: {
		textDecorationLine: 'line-through',
		textDecorationStyle: 'solid'
	},
	thumb: {
		borderRadius: 12,
	}
});
