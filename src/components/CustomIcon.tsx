import React from 'react';
import { StyleProp, TextStyle } from 'react-native';
import { createIconSetFromFontello } from 'react-native-vector-icons';
const fontelloConfig = require('../../assets/fonts/config.json');
const Icon = createIconSetFromFontello(fontelloConfig);

export default ({
	name,
	size,
	style,
}:CustomIconProps) => (
	<Icon
		name={name}
		size={size || 18}
		style={style}
	/>
);

interface CustomIconProps {
	name: string,
	size?: number,
	style?: StyleProp<TextStyle>,
};
