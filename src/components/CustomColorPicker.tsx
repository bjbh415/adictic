import React, { PureComponent } from 'react';
import {
  // Dimensions,
  StyleSheet,
  View,
  FlatList,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class CustomPickerColor extends PureComponent<CustomColorProps> {
  _renderColor = ({ item }: { item: string }) => (
    <TouchableOpacity
      onPress={() => this.props.onPress(item)}
    >
      <View
        style={[
          styles.colorItemContainer,
          (this.props.selectedColor === item) ? styles.colorActive : {},
        ]}
      >
        <View
          style={[
            styles.colorItem,
            { backgroundColor: item }
          ]}
        />
      </View>
    </TouchableOpacity>
  )

  render() {
    const { colors } = this.props;
    
    return (
      <View>
        <FlatList<string>
          horizontal
          data={colors}
          renderItem={this._renderColor}
          keyExtractor={(item) => item}
        />
      </View>
    );
  }
}

// const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
  colorItemContainer: {
    margin: 2,
    width: 48,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
  colorItem: {
    width: 40,
    height: 40,
    borderRadius: 100,
    borderColor: '#ccc',
    borderStyle: "solid",
    borderWidth: 1,
  },
  colorActive: {
    borderRadius: 100,
    borderColor: '#bbb',
    borderStyle: "solid",
    borderWidth: 1,
  }
});

interface CustomColorProps {
  colors: string[],
  selectedColor?: string,
  onPress: (color: string) => void 
};
