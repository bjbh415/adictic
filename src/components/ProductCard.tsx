import React from 'react';
import {
	StyleSheet,
	Dimensions,
	View,
	Image,
	GestureResponderEvent,
} from 'react-native';
import {
	Card,
	CardItem,
	Button,
	Text,
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import CustomIcon from './CustomIcon';
import { TouchableOpacity } from 'react-native-gesture-handler';
	
export default ({
	title,
	imgUri,
	price,
	discountPrice,
	onPress,
}: ProductCardProps) => (
	<TouchableOpacity
		onPress={onPress}
	>
		<Card
		transparent
		style={styles.container}
		>
			<CardItem cardBody>
				<Image
					source={{uri: imgUri}}
					style={[ styles.image ]}
					resizeMode="cover"
				/>
			</CardItem>
			<CardItem cardBody>
				<Grid>
					<Row>
						<Col size={0.85}>
							<Text numberOfLines={2}>{title}</Text>
						</Col>
						<Col size={0.15}>
							<Button
								transparent
								onPress={() => {}}
							>
								<CustomIcon
									name="favorite"
								/>
							</Button>
						</Col>
					</Row>
					<Row>
						<View style={{ flexDirection: 'row' }}>
							
							{discountPrice && (
								<>
									<Text
										note
										style={styles.deprecated}
									>
										{price}$
									</Text>

									<View style={{ width: 12 }} />
													
									<Text note>{discountPrice}$</Text>
								</>
							)}

							{!discountPrice && (
								<Text note>{price}</Text>
							)}
						</View>
					</Row>
				</Grid>
			</CardItem>
		</Card>
	</TouchableOpacity>
);

const { width } = Dimensions.get('screen');
const styles = StyleSheet.create({
	container: {
		width: width * 0.46,
	},
	image: {
		borderRadius: 16,
		width: '100%',
		height: 163,
	},
	deprecated: {
		textDecorationLine: 'line-through',
		textDecorationStyle: 'solid'
	}
});

interface ProductCardProps {
	title?: string,
	imgUri?: string, 
	price?: number, 
	discountPrice?: number,
	onPress: (event: GestureResponderEvent) => void,
};
