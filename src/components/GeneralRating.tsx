import React from 'react';
import {
  // Dimensions,
  StyleSheet,
  View,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import {
  Text
} from 'native-base';

export default ({
  rating,
  valorationsNumber,
}: GeneralRatingProps) => (
  <View style={styles.container}>
			<StarRating
				disabled
				maxStars={5}
				rating={rating}
				starSize={24}
				fullStarColor="#FFE082"
				emptyStarColor="#ccc"
			/>
			<View style={{ width: 16 }} />
			<Text note style={{ fontSize: 18 }}>{`(${valorationsNumber})`}</Text>
  </View>
);

// const { width, height } = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    // justifyContent: 'flex-end',
  },
});

interface GeneralRatingProps {
	rating: number,
	valorationsNumber: number,
};
