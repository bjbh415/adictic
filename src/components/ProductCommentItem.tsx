import React from 'react';
import {
	StyleSheet, View,
	// Dimensions,
} from 'react-native';
import {
	Card,
	CardItem,
	Text,
} from 'native-base';
import StarRating from 'react-native-star-rating';
import { ProductComment } from 'src/interfaces/Product';
import { Row, Grid } from 'react-native-easy-grid';
	
export default ({
	_id,
	title,
	rating,
	comment,
}: ProductComment) => (
	<Card>
		<CardItem header>
			<Grid>
				<Row>
					<Text>{title}</Text>
				</Row>
				<Row>
					<StarRating
						disabled
						maxStars={5}
						rating={rating}
						starSize={18}
						fullStarColor="#FFE082"
						emptyStarColor="#ccc"
					/>
				</Row>
			</Grid>
		</CardItem>
		<CardItem>
			<Text note>{comment}</Text>
		</CardItem>
	</Card>
);

// const { width } = Dimensions.get('screen');
// const styles = StyleSheet.create({});
