
import { createStore } from 'redux';
import Reducers from './reducers'

export default () => {
    let store = createStore(Reducers)
    return store
}
