import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Animated, Easing } from 'react-native';

import WelcomeScreen from '../screens/WelcomeScreen/WelcomeScreen';
import HomeScreen from '../screens/HomeScreen/HomeScreen';
import ProductDetailsScreen from '../screens/ProductDetailsScreen/ProductDetailsScreen';
import CartScreen from '../screens/CartScreen/CartScreen';

const transitionConfig = () => {
	return {
		transitionSpec: {
		duration: 750,
		easing: Easing.out(Easing.poly(4)),
		timing: Animated.timing,
		useNativeDriver: true
		},
		screenInterpolator: (sceneProps: any) => {
		const { layout, position, scene } = sceneProps;

		const thisSceneIndex = scene.index;
		const width = layout.initWidth;

		const translateX = position.interpolate({
			inputRange: [thisSceneIndex - 1, thisSceneIndex],
			outputRange: [width, 0]
		});

		return { transform: [{ translateX }] };
		}
	};
};

const AppRoutes = createStackNavigator(
	{
		WelcomeScreen: {
			screen: WelcomeScreen,
			navigationOptions: () => ({
				header: null,
			}),
		},
		HomeScreen: {
			screen: HomeScreen,
			navigationOptions: () => ({
				header: null,
			}),
		},
		ProductDetailsScreen: {
			screen: ProductDetailsScreen,
			navigationOptions: () => ({
				header: null,
			}),
		},
		CartScreen: {
			screen: CartScreen,
			navigationOptions: () => ({
				header: null,
			}),
		},
	},
	{
		initialRouteName: 'WelcomeScreen',
    transitionConfig,
	}
);

const AppNavigator = createAppContainer(AppRoutes);

export default AppNavigator;
