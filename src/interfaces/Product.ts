export interface ProductComment {
	_id: string,
	title: string,
	rating: number,
	comment: string,
};

export default interface Product {
	_id: string;
	title: string;
	category: string;
	price: number;
	discountPrice?: number;
	imgUri: string;
	photos: string[];
	description: string,
	colors: string[],
	sizes: string[],
	availableNumber: number,
	comments: ProductComment[],
	rating: number[],
};
