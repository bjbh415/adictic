import {
	SET_PRODUCTS_DATA,
	SET_SELECTED_PRODUCT,
	SET_SELECTED_PRODUCT_REVIEW,
	SET_SELECTED_PRODUCT_COLOR,
	SET_SELECTED_PRODUCT_SIZE,
	SET_SELECTED_PRODUCT_NUMBER,
} from '../actions/products';
import Product from 'src/interfaces/Product';

const selectedProduct: ISelectedProduct = {
	_id: '',
	selectedReview: {
		rating: 0,
		title: '',
		comment: '',
	},
	selectedColor: '',
	selectedSize: '',
	selectedNumber: 0,
};

const initialState: IProductsState =  {
	isFeching: false,
	error: false,
	data: [],
	selectedProduct,
};

export default (state = initialState, action: IAction) => {
	const { payload } = action;

	switch(action.type) {
		case SET_PRODUCTS_DATA: {
			return {
				...state,
				data: payload.data,
			}
		}
		case SET_SELECTED_PRODUCT: {
			return {
				...state,
				selectedProduct: {
					...state.selectedProduct,
					_id: payload._id,
				},
			}
		}
		case SET_SELECTED_PRODUCT_REVIEW: {
			return {
				...state,
				selectedProduct: {
					...state.selectedProduct,
					selectedReview: payload.review,
				},
			}
		}
		case SET_SELECTED_PRODUCT_COLOR: {
			return {
				...state,
				selectedProduct: {
					...state.selectedProduct,
					selectedColor: payload.color,
				},
			}
		}
		case SET_SELECTED_PRODUCT_SIZE: {
			return {
				...state,
				selectedProduct: {
					...state.selectedProduct,
					selectedSize: payload.size,
				},
			}
		}
		case SET_SELECTED_PRODUCT_NUMBER: {
			return {
				...state,
				selectedProduct: {
					...state.selectedProduct,
					selectedNumber: payload.number,
				},
			}
		}
		default:
		return state
	}
}

interface IAction {
	type: string,
	payload: any,
};

export interface IProductsState {
	data: Product[],
	isFeching: boolean,
	error: boolean,
	selectedProduct: ISelectedProduct, 
};

export interface ISelectedProduct {
	_id: string,
	selectedReview: IProductReview,
	selectedColor: string,
	selectedSize: string,
	selectedNumber: number,
};

export interface IProductReview {
	rating: number,
	title?: string,
	comment?: string,
}
