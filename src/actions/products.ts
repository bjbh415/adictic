import Product from "src/interfaces/Product";
import { IProductReview } from "src/reducers/products";

export const SET_PRODUCTS_DATA = 'SET_PRODUCTS_DATA';
export const SET_SELECTED_PRODUCT = 'SET_SELECTED_PRODUCT';
export const SET_SELECTED_PRODUCT_REVIEW = 'SET_SELECTED_PRODUCT_REVIEW';
export const SET_SELECTED_PRODUCT_COLOR = 'SET_SELECTED_PRODUCT_COLOR';
export const SET_SELECTED_PRODUCT_SIZE = 'SET_SELECTED_PRODUCT_SIZE';
export const SET_SELECTED_PRODUCT_NUMBER = 'SET_SELECTED_PRODUCT_NUMBER';

export const setProducts = (data: Product[]) => {
  return {
    type: SET_PRODUCTS_DATA,
    payload: {
      data,
    }
  }
};

export const setSelectedProduct = (_id: string) => {
  return {
    type: SET_SELECTED_PRODUCT,
    payload: {
      _id,
    }
  }
};

export const setSelectedProductReview = (review: IProductReview) => {
  return {
    type: SET_SELECTED_PRODUCT_REVIEW,
    payload: {
      review,
    }
  }
};

export const setSelectedProductColor = (color: string) => {
  return {
    type: SET_SELECTED_PRODUCT_COLOR,
    payload: {
      color,
    }
  }
};

export const setSelectedProductSize = (size: string) => {
  return {
    type: SET_SELECTED_PRODUCT_SIZE,
    payload: {
      size,
    }
  }
};

export const setSelectedProductNumber = (number: number) => {
  return {
    type: SET_SELECTED_PRODUCT_NUMBER,
    payload: {
      number,
    }
  }
};

